import Vue from 'vue'
import router from './router/index'
import store from './store/index'
import Vuetify from 'vuetify'
import App from './App'
import firebase from 'firebase/app'
import 'firebase/auth'
import 'vuetify/dist/vuetify.min.css'

Vue.use(Vuetify)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  vuetify: new Vuetify(),
  render: h => h(App),
  created () {
    firebase.initializeApp({
      apiKey: 'AIzaSyBP16RAD6RgL_KBXV5UTcPcQEsAZhCUhxs',
      authDomain: 'ads-app-9839b.firebaseapp.com',
      projectId: 'ads-app-9839b',
      storageBucket: 'ads-app-9839b.appspot.com',
      messagingSenderId: '730160378885',
      appId: '1:730160378885:web:bdc06d07d8c4fe6885186c',
      measurementId: 'G-EXZ60BH1MF'
    })

    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.$store.dispatch('autoLoginUser', user)
      }
    })
  }
})
